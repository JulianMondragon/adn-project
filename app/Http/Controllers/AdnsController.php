<?php

namespace App\Http\Controllers;

use App\Models\Adns;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Helpers\evaluateMatrix; 

class AdnsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $count_mutations = 0;
        $count_no_mutations = 0;
        $adns = Adns::all();
        $total = Count($adns);

        foreach ($adns as $adn) 
        {

            if ($adn->hasmutation == TRUE) 
            {
                $count_mutations += 1;
            }
            else
            {
                $count_no_mutations += 1;
            }
        }
        
        $json['count_mutations'] = $count_mutations;
        $json['count_no_mutations'] = $count_no_mutations;
        if($count_no_mutations == 0)
        {
            $json['radio']= $count_mutations;
        }
        else
        {
            $json['radio']= ($count_mutations / $count_no_mutations);
        }
        return $json;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, evaluateMatrix $evalute)
    { 
        $ev = new $evalute;  
        $array = Arr::get($request, 'dna');
        if($ev->validateRow($array))
        {
          $string = $ev->getMatrixInString($array);
          $adn = new Adns;
          $adn->adn = $string;
          $adn->hasmutation = TRUE;
          $adn->save();
          return TRUE;
        }
        elseif ($ev->validateColumn($array)) 
        {
          $string = $ev->getMatrixInString($array);
          $adn = new Adns;
          $adn->adn = $string;
          $adn->hasmutation = TRUE;
          $adn->save();
          return TRUE;
        }
        elseif ($ev->validateDiagonalPositive($array))
        {
          $string = $ev->getMatrixInString($array);
          $adn = new Adns;
          $adn->adn = $string;
          $adn->hasmutation = TRUE;
          $adn->save();
          return TRUE;
        }
        elseif ($ev->validateDiagonalNegative($array)) 
        {
          $string = $ev->getMatrixInString($array);
          $adn = new Adns;
          $adn->adn = $string;
          $adn->hasmutation = TRUE;
          $adn->save();
          return TRUE;
        }
        else 
        {
            $string = $ev->getMatrixInString($array);
            $adn = new Adns;
            $adn->adn = $string;
            $adn->hasmutation = FALSE;
            $adn->save();
            $json['type'] = 'error 403';
            $json['message'] ="No autorizado";
            return $json;
        }
    }

   
}
