# adn-project

Este es un proyecto creado con Laravel.

Para ejecutar el proyecto en su computadora; Si bien es posible que desee explorar estas opciones en un momento posterior, Laravel proporciona Sail <https://laravel.com/docs/8.x/sail>, una solución incorporada para ejecutar su proyecto Laravel utilizando Docker .

Docker es una herramienta para ejecutar aplicaciones y servicios en "contenedores" pequeños y livianos que no interfieren con el software o la configuración instalados en su computadora local. Esto significa que no tiene que preocuparse por configurar o configurar herramientas de desarrollo complicadas, como servidores web y bases de datos en su computadora personal. Para comenzar, solo necesita instalar Docker Desktop <https://www.docker.com/products/docker-desktop>.

Laravel Sail es una interfaz de línea de comandos liviana para interactuar con la configuración predeterminada de Docker de Laravel. Sail proporciona un excelente punto de partida para construir una aplicación Laravel usando PHP, MySQL y Redis sin requerir experiencia previa en Docker.

Para empezar a trabajar con Laravel es necesario cumplir con los siguientes requisitos iniciales:

Un entorno de desarrollo web: 
 * Apache, IIS, Nginx PHP 5.3 o superior
 * Base de datos: MySQL, Sqlite, Postgresql o sqlserver
 * Librerías php : Mcrypt
 * Composer  <https://getcomposer.org/>


Intrucciones para correr localmente el proyecto: 

1.- Una vez que se tiene la carpeta descomprimida adn-project es recomendable que se establezca en la raiz de tu equipo

    (Nota: es necesario contar con un gestor de base de datos instalado y se debe crear una base de datos llamada mutations)

2.- Dentro de la carpeta adn-project existe un archivo .env, se debe abrir este archivo con cualquier editor de texto y modificar las cadenas de conexion y el servidor de base de datos preferido, anexo ejemplo: 

    DB_CONNECTION=mysql
    DB_HOST=<servidor>
    DB_PORT=3306
    DB_DATABASE=mutations
    DB_USERNAME=<usuario>
    DB_PASSWORD=<contraseña>

2.- Debes abrir una terminal y dirigirte a la ubicacion del proyecto

3.- Estando dentro de la carpeta debes ejecutar los siguientes comandos:

    php artisan migrate 
    (Este comando solo se ejecuta la primera vez que vayas a correr el proyecto y lo que hace es que ejecuta las migraciones creando las tabalas necesarias para poder consumir y guardar los datos)

    php artisan serve 
    (Este comando sirve para levantar el servidor de una aplicación, es decir, para hacer correr una aplicación con el servidor que viene incluido en Laravel)

 Se abrira el navegador establecido con direccion del servidor y el puerto, por lo regular es http://127.0.0.1:8000/ el cual te mostrara la pagina de inicio de la Api con una pequeña documentacion.

 4.- Para consumir esta Api son solicitudes http Get/Post se puede usar la herramienta de Postman con la cual haremos una peticion Get http://127.0.0.1:8000/mutations y devolvera un resumen de los datos guardados un un radio de las mutaciones encontras en un formato json, anexo ejemplo: 

    {
    "count_mutations": 3,
    "count_no_mutations": 1,
    "radio": 3
    }

 5.- Para evaluar una cadena se debe hacer una peticion Post http://127.0.0.1:8000/mutation y mandar un arreglo en formato JSON, anexo ejemplo: 

    {
    "dna":["ATGCGA", "CAGTGC", "TTATTT", "AGAAGG", "GCGTCA", "TCACTG"]
    }
    
