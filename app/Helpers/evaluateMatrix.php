<?php

namespace App\Helpers;


/**
 * En esta clase se encuentran las funciones para validar la matrix
 */
class evaluateMatrix 
{
	
	/**
     * Mediante expreciones regulares se valida que haya una 
     * secuencia de 4 letras iguales
     */
    static function validateString($string, $sequences = array('/A{4}/', '/C{4}/', '/T{4}/', '/G{4}/'))
    {
      foreach ($sequences as $sequence) {
        if(preg_match($sequence, $string)) {
          return TRUE;
        }
      }
    }

    /*
    * Valida cada una de las filas del arreglo con la funcion
    * validateString
    */
    static function validateRow($array)
    {
      foreach ($array as $value) 
      {
        if(static::validateString($value)){
          return TRUE;
        };
      }
      return FALSE;
    }

    /*
    * Toma las letras correspondientes a la fila y los convierte en
    * una cadena de texto y usa la funcion validar cadena para 
    * encontrar si hay mutacion   
    */
    static function validateColumn($array)
    {
      $count = 0;
      do {
        $newstring = '';
        foreach ($array as $value) {
          $newstring = $newstring . $value[$count];
        }
        if(static::validateString($newstring)){
          return TRUE;
        }
        $count += 1;

      } while ($count < sizeof($array));
      return FALSE;
    }

    /*
    * Toma las cadenas oblicuas posibles segun la matriz partiendo 
    * de una linea central hacia la derecha con cuatro caracteres
    * y valida esas cadenas con la funcion validateString  
    */
    static function validateDiagonalPositive($array)
    {
      $count = 0;
      do 
      {
        $positiveString = '';
        if ((strlen($array[0]) - $count) <= 3)
        {
          break;
        }
        foreach ($array as $key => $value) 
        {
            $suma = $key + $count;
            if($suma < strlen($value))
            {
                $positiveString = $positiveString . $value[$suma];
            }
        }
        if(static::validateString($positiveString))
        {
          return TRUE;
          break;
        }
        $count += 1;
      } while ($count < sizeof($array));
      return FALSE;
    }

    /*
    * Toma las cadenas oblicuas posibles segun la matriz partiendo 
    * de una linea central hacia la izquierda con cuatro caracteres
    * y valida esas cadenas con la funcion validateString  
    */
    static function validateDiagonalNegative($array)
    {
      $count = 0;
      do 
      {
        $negativeString = '';
        if ((sizeof($array) - $count) <= 3)
        {
          break;
        }
        foreach ($array as $key => $value) 
        {
          $resta = $key - $count;
          if($resta >= 0  && $resta < strlen($value)){
            $negativeString = $negativeString . $value[$resta];
          };
        }
        if(static::validateString($negativeString))
        {
          return TRUE;
          break;
        }
        $count += 1;
      } 
      while ($count < sizeof($array));
      return FALSE;
    }

    static function getMatrixInString($array)
    {
      $string = ''; 
      foreach ($array as $value) 
      {

        $string = $string . $value . ',';
      }
      $rest = substr($string, 1, -1);
      return $rest;
    }
}